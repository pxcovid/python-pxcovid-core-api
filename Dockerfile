FROM python:3.9

ARG YOUR_ENV

WORKDIR /python_pxcovid_core_api
COPY pyproject.toml poetry.lock /python_pxcovid_core_api/

RUN pip install --user poetry
ENV PATH="${PATH}:/root/.local/bin"
RUN poetry install && poetry update

RUN poetry export --without-hashes > requirements.txt
RUN pip install -r requirements.txt

COPY ./python_pxcovid_core_api ./python_pxcovid_core_api

CMD [ "python", "./python_pxcovid_core_api/app.py" ]