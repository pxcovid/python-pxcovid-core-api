# PXCovid Core API

Este projeto é uma API feita em Python3.9. Está sendo desenvolvimento com a base [python-flask-simple](https://gitlab.com/MatheusMaia/python-flask-simple). 
# Informações do Projeto

Nome do Projeto: **PXCovid Core APIt**

Autor: [@MatheusMaia](https://gitlab.com/MatheusMaia)

Principais Bibliotecas/Tecnologias: 
- **Python** ( _3.9_ )
- Para gerenciamento de bibliotecas: **Poetry** ( _1.1.8_ )
- Para criação da API: **Flask** ( _2.0.1_ ) 



# Como Rodar

- ## Local 
    - Requisitos:
        - Python ( _^3.9_ )
        - Poetry ( _^1.1.8_ )

```bash
git clone https://gitlab.com/pxcovid/python-pxcovid-core-api.git

cd python-pxcovid-core-api/
poetry install 
poetry update

cd python_pxcovid_core_api
poetry run Flask run -h localhost -p 3000
```



- ## Produção
    - Requisitos:
        - Git
        - Docker

```bash
git clone https://gitlab.com/pxcovid/python-pxcovid-core-api.git

cd python-pxcovid-core-api

docker build -t python-pxcovid-core-api .
docker run -p 3000:3000 -d --name python-pxcovid-core-api python-pxcovid-core-api 
```


# Comandos Básicos - Handsbook

- ## Git Comandos Básicos

#### Para _configurar_ sua identidade git
```cmd
git config --global user.name "Fulano de Tal"
git config --global user.email fulanodetal@exemplo.br
```

#### Para _criar_ uma branch
```cmd
git checkout -b ＜new-branch＞ ＜existing-branch＞
```


#### Para fazer _merge_
```cmd
git checkout master
git pull

git checkout new-feature
git pull

git merge 'master'
# Resolvendo os conflitos
git add *
git commit -m "[conflict] merge 'master' into 'new-feature'"
git push

git checkout master
git pull
git merge new-feature
```


#### Para mais informações 
```cmd
git --help
```
ou através da documentação oficial: [git-scm.com](https://git-scm.com/doc)

- ## Python Comandos Básicos

#### Para instalar o Poetry
```cmd
pip install --user poetry
```


- ## Poetry Comandos Básicos



#### Para _criar_ um projeto
```cmd
poetry new python-flask-simple
```

#### Para _adicionar/instalar_ dependencias
```cmd
poetry add flask
```

#### Para fazer o _build_
```cmd
poetry build
```

#### Para Executar
```cmd
# Entrar na pasta que está o app.py
poetry run Flask run -h localhost -p 3000
```

#### Para Exportar o _requirements.txt_

```cmd
poetry export --without-hashes > requirements.txt
```

#### Para mais informações 
```cmd
poetry --help
```
ou através da documentação oficial: [poetry.org](https://python-poetry.org)



- ## Flask Comandos Básicos

#### Para Executar
```cmd
# Deve entrar na pasta que está o app.py
flask run -h localhost -p 3000
```
#### Para mais informações 
```cmd
flask --help
```

ou através da documentação oficial: [flask.palletsprojects.com](https://flask.palletsprojects.com/en/2.0.x/)

