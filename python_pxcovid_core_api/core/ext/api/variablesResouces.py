from flask import Blueprint

bp = Blueprint("variaveis", __name__)

url_prefix = "/api/v1/data"

@bp.route(url_prefix + "/variable/diabetes", methods=['GET'])
def diabetesResource():
    return {
        "label": "Diabetes",
        "alias": "diabetes",
        "content": [
            {
                "id": 0,
                "label": "Não",
                "alias": "nao"
            },
            {
                "id": 1,
                "label": "Sim",
                "alias": "sim"
            }
        ],
        "total": 2
    }, 200


@bp.route(url_prefix + "/variable/leucocitos", methods=['GET'])
def leucocitoResource():
    return {
    "label": "Leucócitos",
    "alias": "leucocito",
    "content": [
        {
            "id": 0,
            "label": "Menor que 7.500",
            "alias": "maior_que_7500"
        },
        {
            "id": 1,
            "label": "Entre 7.500 e 11.200",
            "alias": "entre_7500_e_11200"
        },
        {
            "id": 2,
            "label": "Entre 11.200 e 15.100",
            "alias": "entre_11200_e_15100"
        },
        {
            "id": 3,
            "label": "Maior 15.100",
            "alias": "maior_15100"
        }
    ],
    "total": 3
}, 200

@bp.route(url_prefix + "/variable/plaquetas", methods=['GET'])
def plaquetasResource():
    return {
    "label": "Plaquetas",
    "alias": "plaquetas",
    "content": [
        {
            "id": 0,
            "label": "Menor que 190.000",
            "alias": "menor_que_190000"
        },
        {
            "id": 1,
            "label": "Entre 190.000 e 259.000",
            "alias": "entre_190000_e_259000"
        },
        {
            "id": 2,
            "label": "Entre 259.000 e 334.000",
            "alias": "entre_259000_e_334000"
        },
        {
            "id": 3,
            "label": "Maior que 334.000",
            "alias": "maior_que_334000"
        }
    ],
    "total": 4
}, 200


@bp.route(url_prefix + "/variable/hemoglobinas", methods=['GET'])
def hemoglobinaResource():
    return {
    "label": "Hemoglobina",
    "alias": "hemoglobina",
    "content": [
        {
            "id": 0,
            "label": "Menor 8",
            "alias": "menor_8"
        },
        {
            "id": 1,
            "label": "Entre 8 e 11",
            "alias": "entre_8_e_11"
        },
        {
            "id": 2,
            "label": "Entre 11 e 12",
            "alias": "entre_11_e_12"
        },
        {
            "id": 3,
            "label": "Maior 13",
            "alias": "maior_13"
        }
    ],
    "total": 4
}, 200

@bp.route(url_prefix + "/variable/idades", methods=['GET'])
def idadeResource():
    return {
    "label": "Idade (em anos)",
    "alias": "idade",
    "content": [
        {
            "id": 0,
            "label": "Menor 40",
            "alias": "menor_40"
        },
        {
            "id": 1,
            "label": "Entre 40 e 60",
            "alias": "entre_20_e_30"
        },
        {
            "id": 2,
            "label": "Maior 60",
            "alias": "maior_60"
        }
    ],
    "total": 7
}, 200

@bp.route(url_prefix + "/variable/desfechos", methods=['GET'])
def desfechoResource():
    return {
    "label": "Desfecho",
    "alias": "desfecho",
    "content": [
        {
            "id": 0,
            "label": "Alta na UTI",
            "alias": "alta_na_uti"
        },
        {
            "id": 1,
            "label": "Obito na UTI",
            "alias": "obito_na_uti"
        }
    ],
    "total": 2
}, 200
