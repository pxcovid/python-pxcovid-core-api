from .variablesResouces import bp as variablesBp
from .algorithmResource import bp as naiveBayesBp


def init_app(app):
    app.register_blueprint(variablesBp)
    app.register_blueprint(naiveBayesBp)
