from flask import Blueprint, jsonify, request
import joblib as joblib

clf = joblib.load('python_pxcovid_core_api/core/ext/dataScienceModels/covid_naivebayes.pk1')

bp = Blueprint("naivebayes", __name__)

url_prefix = "/api/v1/naivebayes/"

@bp.route(url_prefix + "pacientes", methods=['GET'])
def diabetesResource():
    covid_idade = float(request.args.get('idade'))
    covid_diabetes = float(request.args.get('diabetes'))
    covid_hemoglobina = float(request.args.get('hemoglobina'))
    covid_leucocito = float(request.args.get('leucocito'))
    covid_plaquetas = float(request.args.get('plaquetas'))


    event = [covid_idade, covid_diabetes, covid_hemoglobina, covid_leucocito, covid_plaquetas]
    target_names = ['Obito na UTI', 'Alta da UTI']

    result = {}

    prediction = clf.predict([event])[0]
    probas = zip(target_names, clf.predict_proba([event])[0])

    result['diagnostic'] = target_names[prediction]
    
    probas_tuple = tuple(probas)

    # result['probas'] = probas_tuple

    
    prob0 = probas_tuple[0]
    prob1 = probas_tuple[1]

    if ( prob0[0] == result['diagnostic']):
        result['value'] = prob0[1]
    elif ( prob1[0] == result['diagnostic']):
        result['value'] = prob1[1]


    if ( result['value'] > 0.9 ):
        result['level'] = "ALTA"
    elif ( result['value'] > 0.5 ):
        result['level'] = "MEDIA"
    else:
        result['level'] = "BAIXA"


    return jsonify(result), 200 

