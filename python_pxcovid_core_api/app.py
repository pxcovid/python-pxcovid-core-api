from flask import Flask
# from core.ext import api
from core.ext import api


def create_app():
    app = Flask(__name__)
    
    api.init_app(app)
    return app

if __name__ == "__main__":
    create_app().run(host='0.0.0.0', port=3000)


